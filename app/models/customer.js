exports.definition = {
	config: {
		columns: {
		    "id": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "place": "TEXT",
		    "address": "TEXT",
		    "telephone" :"TEXT",
		    "type": "TEXT",
		    "name": "TEXT",
		    "ischecked": "INTEGER",
		    "latitude":"TEXT",
		    "longitude":"TEXT"
		},
		adapter: {
			type: "sql",
			collection_name: "customer"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			
            updateById : function(ischecked,id) {
                var db = Titanium.Database.open('_alloy_');
                var query = "Update customer set  ischecked = ?  where id=?";
                db.execute(query,ischecked, id);
                // plus ,utk parameter
                db.close();
                // this.fetch();
                // this.trigger('sync'); // bind dengan list
            },
            updateAllStatus : function(){
                var db = Titanium.Database.open('_alloy_');
                var query = "Update customer set ischecked = 0";
                db.execute(query);
                db.close();
            }
		});

		return Collection;
	}
};