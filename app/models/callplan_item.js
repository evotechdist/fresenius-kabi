exports.definition = {
	config: {
		columns: { 
		    "id": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "event_name":"TEXT",
		    "visit_date": "TEXT",
		    "visit_time":"TEXT",
		    "agenda": "TEXT",
		    "notes": "TEXT",
		    "status": "INTEGER",
		    "submited_time": "TEXT",
		    "signature": "TEXT",
		    "submited_latitude": "TEXT",
		    "submited_longitude": "TEXT",
		    "created_date": "TEXT",
            "is_approved": "INTEGER",
            "id_user": "TEXT",
            "approved_by": "TEXT"
		    
		},
		adapter: {
			type: "sql",
			collection_name: "callplan_item"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {

			updateById : function(status,id) {
                var db = Titanium.Database.open('_alloy_');
                var query = "Update callplan_item set status = ?  where id=?";
                db.execute(query,status, id);
                // plus ,utk parameter
                db.close();
                // this.fetch();
                // this.trigger('sync'); // bind dengan list
            },
            updateApproveById : function(is_approved,id) {
                var db = Titanium.Database.open('_alloy_');
                var query = "Update callplan_item set is_approved = ?  where id=?";
                db.execute(query,is_approved, id);
                // plus ,utk parameter
                db.close();
                // this.fetch();
                // this.trigger('sync'); // bind dengan list
            }
		});

		return Collection;
	}
};