 
// id_role 0 -> no have
// id_role 1 -> reps
// id_role 2 -> manager
// id_role 3 -> supervisor
var user = [{
    id : 'u1',
    username : 'user1',
    password : 'user1',
    email : 'user1@fresenius.com',
    id_role : 1,
    id_manager : 'm1'
},
{id : 'u2',
    username : 'user2',
    password : 'user2',
    email : 'user2@fresenius.com',
    id_role : 1,
    id_manager : 'm1'
},
{id : 'u3',
    username : 'user3',
    password : 'user3',
    email : 'user3@fresenius.com',
    id_role : 1,
    id_manager : 'm2'
},
{id : 'u4',
    username : 'user4',
    password : 'user4',
    email : 'user4@fresenius.com',
    id_role : 1,
    id_manager : 'm2'
},
{id : 'm1',
    username : 'manager1',
    password : 'manager1',
    email : 'manager1@fresenius.com',
    id_role : 2,
    id_manager : 's1'
},
{id : 'm2',
    username : 'manager2',
    password : 'manager2',
    email : 'manager2@fresenius.com',
    id_role : 2,
    id_manager : 's1'
},
{id : 's1',
    username : 'superior1',
    password : 'superior1',
    email : 'superior11@fresenius.com',
    id_role : 3,
    id_manager : '0'
},
];
var customer =[
{
    // id : 'c1',
    place : 'RS. Sumber Waras',
    address : 'Jl. Kyai Tapa No.1',
    type : 'A',
    name : 'Dr. Antonious',
    ischecked : 0,
    latitude : "-6.1673795",
    longitude : "106.7961087",
    telephone : 08194906706
},
{
    // id : 'c2',
    place : 'Rs. Royal Taruna',
    address : 'Jl. Daan Mogot No.34',
    type : 'A',
    name : 'Dr. Putra',
    ischecked : 0,
    latitude : "-6.1666791",
    longitude : "106.7832436",
    telephone : 08194906707
},
{
    // id : 'c3',
    place : 'RS. M.H Tamrin',
    address : 'Jl. Daan Mogot Km 17',
    type : 'B',
    name : 'Dr. Kony',
    ischecked : 0,
    latitude : "-6.158369",
    longitude : "106.696844",
    telephone : 08194906708
    
},
{
    // id : 'c4',
    place : 'RS. Dharmais',
    address : 'Jl. Letjen S.Parman',
    type : 'C',
    name : 'Dr. Crane',
    ischecked : 0,
    latitude : "-6.187216",
    longitude : "106.7953653",
    telephone : 08194906709
},{
    // id:'c5',
    place : 'RS. Dharmais',
    address : 'Jl. Letjen S.Parman',
    type : 'D',
    name : 'Dr. CliGad',
    ischecked : 0,
    latitude : "-6.187216",
    longitude : "106.7953653",
    telephone : 08194906710
}
];
var role = [{
    id: 0,
    position : 'none'
},{
   id : 1,
   position : 'reps' 
},{
    id:2,
    position : 'manager'
},{
    id:3,
    position : 'supervisor'
}];
var status = [{
    id : 1,
    description : 'Kunjungan'
},{
    id : 2,
    description : 'Tidak ada orang'
},{
    id : 3,
    description : 'Reschedule'
}];



exports.status = status;
exports.role = role;
exports.customer = customer;
exports.user = user;
 