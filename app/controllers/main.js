var args = $.args;
var collection_callplan_header = Alloy.createCollection('callplan_header');
var collection_callplan_item = Alloy.Collections.callplan_item;
var collection_callplan_customer = Alloy.createCollection('callplan_cust');
var collection_user = Alloy.createCollection('user');
var collection_customer = Alloy.createCollection('customer');
var statusResult = 1;
var idSelected,
    lat_hospital,
    lng_hospital,
    address_hospital,
    name_hospital;
$.pickerMenu.addEventListener('change', function(e) {
    statusResult = e.rowIndex + 1;
});
var idCustomer = [];
function load() {
    collection_callplan_item.fetch({
        query : "select visit_date, visit_time, event_name,is_approved from callplan_item where is_approved = 1 order by visit_date",
        silent : true
    });
    console.log("debug .. " + JSON.stringify(collection_callplan_item));
    // alert("debug .. "+JSON.stringify(collection_callplan_item));
    if (collection_callplan_item.length != 0) {
        var item = collection_callplan_item.toJSON();
        // alert(item);
        console.log("debug .. " + JSON.stringify(item));
        var arr = _.groupBy(item, function(callItem) {
            return callItem.visit_date;
        });
        var sections = [];
        var items = [];
        for (var key in arr) {
            var args = {
                key : key
            };
            var section = Ti.UI.createListSection({
                headerView : Alloy.createController("headerList", args).getView(),
                // headerTitle : key
            });
            sections.push(section);
            items = [];
            collection_callplan_item.fetch({
                query : "select id,agenda,event_name,visit_time, status, is_approved from callplan_item where visit_date = '" + key + "' order by visit_time",
                silent : true
            });
            if (collection_callplan_item.length != 0) {
               
                for (var i = 0; i < collection_callplan_item.length; i++) {

                    var status = collection_callplan_item.at(i).get("status");

                    var is_approved = collection_callplan_item.at(i).get('is_approved');
                    var agenda = collection_callplan_item.at(i).get("agenda");
                   

                    var icon = agenda.charAt(0);
                    if (is_approved == 1) {
                        if (status == 0) {
                            items.push({
                                bindTime : {
                                    text : collection_callplan_item.at(i).get("visit_time")
                                },
                                bindTitle : {
                                    text : collection_callplan_item.at(i).get("event_name")
                                },
                                bindIcon : {
                                    text : icon
                                },
                                id : {
                                    text : collection_callplan_item.at(i).get('id')
                                },
                                selected : {
                                    backgroundColor : "#fff"
                                },
                                properties : {
                                    backgroundColor : "#fff"
                                }
                            });
                       
                        } else if (status == 1) {
                            items.push({
                                bindTime : {
                                    text : collection_callplan_item.at(i).get("visit_time")
                                },
                                bindTitle : {
                                    text : collection_callplan_item.at(i).get("event_name")
                                },
                                bindIcon : {
                                    text : icon
                                },
                                id : {
                                    text : collection_callplan_item.at(i).get('id')
                                },
                                selected : {
                                    backgroundColor : "#fff"
                                },
                                bindImage : {
                                    image : "/doneicon.png"
                                },
                                properties : {
                                    backgroundColor : "#fff"
                                }
                            });
                       
                        } else {
                            items.push({
                                bindTime : {
                                    text : collection_callplan_item.at(i).get("visit_time")
                                },
                                bindTitle : {
                                    text : collection_callplan_item.at(i).get("event_name")
                                },
                                bindIcon : {
                                    text : icon
                                },
                                id : {
                                    text : collection_callplan_item.at(i).get('id')
                                },
                                selected : {
                                    backgroundColor : "#fff"
                                },
                                bindImage : {
                                    image : "/cancelicon.png"
                                },
                                properties : {
                                    backgroundColor : "#fff"
                                }
                            });
                        }
                    }
                }
            }
            console.log("info .. " + JSON.stringify(items));
            section.setItems(items);

            $.listView.sections = sections;
        }
    }
    
}

var tempSection = "temp",
    tempItem = "temp",
    indextemp;
function onItemClickList(e) {
    $.viewDetail.visible = true;
    $.viewMenu.visible = true;

    var section = $.listView.sections[e.sectionIndex];
    var item = section.getItemAt(e.itemIndex);
    var id = item.id.text.toString();
    if (tempItem != "temp") {
        tempItem.selected.backgroundColor = "#fff";
        tempSection.updateItemAt(indextemp, tempItem);
    }
    tempSection = section;
    tempItem = item;
    indextemp = e.itemIndex;
    item.selected.backgroundColor = "#0063be";
    section.updateItemAt(e.itemIndex, item);
    tempSection = section;
    tempItem = item;
    bindInfo(item.bindTime.text, section.headerTitle, e.itemIndex, id);
}

function bindInfo(time, date, index, id) {
    idSelected = id;
    collection_callplan_item.fetch({
        silent : true,
        query : "select callcust.id_callplan, callitem.id iditem, callcust.id_customer, cust.id idcust, cust.name,cust.place,cust.address, cust.latitude, cust.longitude, callitem.event_name , callitem.visit_date, callitem.visit_time,callitem.agenda from callplan_item callitem JOIN callplan_cust callcust ON callitem.id=callcust.id_callplan JOIN customer cust ON cust.id = callcust.id_customer where callcust.id_callplan =  '" + id + "' and callcust.id_customer = cust.id"
    });
    if (collection_callplan_item.length != 0) {
        var title = collection_callplan_item.at(0).get("event_name");
        var place = collection_callplan_item.at(0).get("place");

        var arrName = [];
        var agenda = collection_callplan_item.at(0).get('agenda');
        lat_hospital = collection_callplan_item.at(0).get("latitude");
        lng_hospital = collection_callplan_item.at(0).get("longitude");
        address_hospital = collection_callplan_item.at(0).get("address");
        name_hospital = collection_callplan_item.at(0).get("place");
        for (var i = 0; i < collection_callplan_item.length; i++) {
            arrName.push(collection_callplan_item.at(i).get("name").toString());
        }
        $.lblTitle.text = title;
        $.lblPlace.text = place;
        $.lblName.text = arrName.toString();
        $.lblAgenda.text = agenda;
    }
}

function onClickFinish() {
    console.log("debuging .. status " + statusResult + " id " + idSelected);
    collection_callplan_item.updateById(statusResult, idSelected);

    init();
}

function onClickSignature() {
    Alloy.createController("signature").getView().open();
}

function onClickDial(data) {
    try {
        var no = "081904906706";
        var intent = Ti.Android.createIntent({
            action : Ti.Android.ACTION_DIAL,
            data : "'tel:" + no + "'"
        });
        Ti.Android.currentActivity.startActivity(intent);
    } catch(err) {
        console.log(err);
    }

}

function onClickCamera() {
    try {
        Titanium.Media.showCamera({
            success : function(event) {
                var capturedImg = event.media;
                if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {

                }
            },
            cancel : function() {
            },
            error : function() {
            }
        });
    } catch(err) {
        console.log(err);
    }
}

function onClickMaps() {
    var args = {
        lat : lat_hospital,
        lng : lng_hospital,
        place : name_hospital,
        address : address_hospital,
    };
    Alloy.createController("maps", args).getView().open();

}

function onClickFile() {
    try {
        var filename = "nodejs_tutorial.pdf";
        var file = Titanium.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, filename);
        Ti.Android.currentActivity.startActivity(Ti.Android.createIntent({
            action : Ti.Android.ACTION_VIEW,
            type : 'application/pdf',
            data : file.getNativePath()
        }));
    } catch (err) {
        var alertDialog = Titanium.UI.createAlertDialog({
            title : 'No PDF Viewer',
            message : 'We tried to open a PDF but failed. Do you want to search the marketplace for a PDF viewer?',
            buttonNames : ['Yes', 'No'],
            cancel : 1
        });
        alertDialog.show();
        alertDialog.addEventListener('click', function(evt) {
            if (evt.index == 0) {
                Ti.Platform.openURL('http://search?q=pdf');
            }
        });
    }

}

function init() {
    collection_callplan_item.fetch({
        silent : true
    });
    console.log("JSON stringify " + JSON.stringify(collection_callplan_item));
    if (collection_callplan_item.length != 0) {
        load();
    }
}

init(); 