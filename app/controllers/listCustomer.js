var args = $.args;
var place = args.place;
var collection_customer = Alloy.Collections.customer;
var arrName = [];
var arrId = [];
var arrStatus = [];
var win = $.win;
var status;

function onItemClickList(e) {

    var section = $.listView.sections[e.sectionIndex];
    var item = section.getItemAt(e.itemIndex);
    console.log("cek bindswitch value " + typeof (JSON.parse(item.bindSwitch.value)));

    if (JSON.parse(item.bindSwitch.value) == true) {
        status = 0;
        item.bindSwitch.value = false;
        section.updateItemAt(e.itemIndex, item);
        arrStatus[e.itemIndex] = status;
        console.log("index false = " + arrStatus[e.itemIndex]);
    } else {
        status = 1;
        item.bindSwitch.value = true;
        section.updateItemAt(e.itemIndex, item);
        arrStatus[e.itemIndex] = status;
        console.log("index true = " + arrStatus[e.itemIndex]);
    }
}

function functionTransform(model) {
    var jsonModel = model.toJSON();
    if (jsonModel.ischecked == 1) {
        jsonModel.ischecked = true;
        // value == 0
        console.log("ischecked =1");
    } else if (jsonModel.ischecked == 0) {
        jsonModel.ischecked = false;
        console.log("ischecked =0");
    }
    return jsonModel;
}

function back() {
    win.close();
}

function onClickCancel() {

}

function onClickDone() {
    for (var i = 0; i < collection_customer.length; i++) {
        collection_customer.updateById(arrStatus[i], arrId[i]);
    }

    win.close();
    args.parent.trigger('setGuest');

}

function init() {
    collection_customer.fetch({
        query : "select id,name, ischecked from customer where place= '" + place + "' "
    });
    // console.log("N " + JSON.stringify(collection_customer));
    for (var i = 0; i < collection_customer.length; i++) {
        arrId.push(collection_customer.at(i).get("id"));
        arrStatus.push(collection_customer.at(i).get("ischecked"));
    }

    console.log(JSON.stringify(arrId) + "\n" + JSON.stringify(arrStatus));
}

init();
