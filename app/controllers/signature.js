var Paint = require('ti.paint');

var win = $.win;
var toast = Ti.UI.createNotification({
    message : "Signature success ..",
    duration : Ti.UI.NOTIFICATION_DURATION_LONG
});
var paintView = Paint.createPaintView({
    top : 0,
    right : 0,
    bottom : 80,
    left : 0,
    // strokeWidth (float), strokeColor (string), strokeAlpha (int, 0-255)
    strokeColor : '#0f0',
    strokeAlpha : 255,
    strokeWidth : 10,
    eraseMode : false
});
win.add(paintView);
function back(){
    win.close();
}
function onClickClear() {
    paintView.clear();
}

function onClickDone() {
    // var dir = Titanium.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, 'signature');
    
    // dir.createDirectory();
    
    // f.move('mysubdir/myfile.txt');
    // // delete the directory
    // if (dir.deleteDirectory() == false) {
        // Ti.API.info('You cannot delete a directory containing files');
        // dir.deleteDirectory(true);
        // // force a recursive directory, which will delete contents
    // }
    
    // // clean the cache directory
    // var cacheDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, "/");
    // cacheDir.deleteDirectory(true);

///storage/emulated/0/com.evotech.freseniuskabi/signature/signature.png
///storage/emulated/0/com.evotech.freseniuskabi/signature.png
    
    // var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, filename);
    var filename = "signature.png";
    var file = Titanium.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, "signature/"+filename);
    if (file.exists()) {
        file.deleteFile();
    }
    console.log(".z.z.z.z.z.z "+paintView.toImage());
    file.write(paintView.toImage());
    var path = file.nativePath;
    Ti.API.info('path of image' + path);
    // alert('Image Save');
    toast.show();
    win.close();

}

win.open();
