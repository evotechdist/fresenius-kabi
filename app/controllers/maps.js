// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var lat = args.lat;
var lng = args.lng;
var place = args.place;
var address = args.address;
var MapModule = require('ti.map');
var win = $.win;

var mapview = MapModule.createView({
    mapType : MapModule.NORMAL_TYPE,
    region : {
        latitude : lat,
        longitude : lng,
        latitudeDelta : 0.04, // semakin mendekati 1 semakin zoom out
        longitudeDelta : 0.04,
        userLocation : true,
        userLocationButton : true
    },
});
var annotation = MapModule.createAnnotation({
    latitude : lat,
    longitude : lng,
    title : place,
    subtitle : address,
    animate : true,
    pincolor : MapModule.ANNOTATION_RED,
});
mapview.addAnnotation(annotation);
mapview.top = 0;
// mapview.height = 200;
mapview.height = Titanium.UI.FILL;
win.add(mapview); 