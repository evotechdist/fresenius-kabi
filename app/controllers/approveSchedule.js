var args = $.args;
var place = args.place;
var collection_callplan_item = Alloy.Collections.callplan_item;
var collection_customer = Alloy.createCollection("customer");
var toast = Ti.UI.createNotification({
    message : "Sukses ..",
    duration : Ti.UI.NOTIFICATION_DURATION_LONG
});
var arrName = [];
var arrId = [];
var arrIs_approved = [];
var win = $.win;
var is_approved = 0;
function onItemClickList(e) {

    var section = $.listView.sections[e.sectionIndex];
    var item = section.getItemAt(e.itemIndex);
    var x = item.bindSwitch.image.charAt(1);
    if (x == "n") {
        is_approved = 1;
        item.bindSwitch.image = "/approveicon.png";
        section.updateItemAt(e.itemIndex, item);
        arrIs_approved[e.itemIndex] = is_approved;

    } else if (x == "a") {
        is_approved = 0;
        item.bindSwitch.image = "/declineicon.png";
        section.updateItemAt(e.itemIndex, item);
        arrIs_approved[e.itemIndex] = is_approved;
    } else {
        is_approved = 1;
        item.bindSwitch.image = "/approveicon.png";
        section.updateItemAt(e.itemIndex, item);
        arrIs_approved[e.itemIndex] = is_approved;

    }
}

function functionTransform(model) {
    var jsonModel = model.toJSON();
    jsonModel.is_approved = "/netralicon.png";
    return jsonModel;
}

function back() {
    win.close();
}

function onClickDecline() {

}
$.on("done", function(){
    for (var i = 0; i < collection_callplan_item.length; i++) {
        collection_callplan_item.updateApproveById(arrIs_approved[i], arrId[i]);
        // alert("debug ... "+JSON.stringify(collection_callplan_item)+"\n"+arrIs_approved[i]+"\n"+arrId[i]);
    } 
    init();
    toast.show();
});

var arrName = [];
// var items = [];
function init() {
    collection_callplan_item.fetch({
        // // silent:true,
        query : "select  callitem.is_approved, callcust.id_callplan, callitem.id iditem, group_concat(cust.name, ', ') as name ,cust.place, callitem.event_name , callitem.visit_date, callitem.visit_time,callitem.agenda from callplan_item callitem JOIN callplan_cust callcust ON callitem.id=callcust.id_callplan JOIN customer cust ON cust.id = callcust.id_customer where is_approved = 0 and callcust.id_callplan =  callitem.id and callcust.id_customer = cust.id group by callitem.id"
    });
    // console.log("z.z.z.z " + JSON.stringify(collection_callplan_item));
    
    for (var i = 0; i < collection_callplan_item.length; i++) {
        arrName.push(collection_callplan_item.at(i).get("name").toString());
        arrId.push(collection_callplan_item.at(i).get("iditem"));
        arrIs_approved.push(collection_callplan_item.at(i).get("is_approved"));
    }
}

init();
