// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var collection_customer = Alloy.Collections.customer;
function load() {
    collection_customer.fetch({
        query : "select name, type, telephone,place from customer",
        silent : true
    });

    if (collection_customer.length != 0) {
        var item = collection_customer.toJSON();
        var arr = _.groupBy(item, function(customer) {
            return customer.place;
        });
        var sections = [];
        var items = [];
        for (var key in arr) {

            var args = {
                key : key
            };

            var section = Ti.UI.createListSection({
                headerView : Alloy.createController("headerList", args).getView()

            });
            items = [];
            collection_customer.fetch({
                query : "select name, type, telephone from customer where place ='" + key + "'",
                silent : true
            });

            for (var i = 0; i < collection_customer.length; i++) {
                var name = collection_customer.at(i).get("name").toString();
                var type = collection_customer.at(i).get('type').toString();
                var telephone = collection_customer.at(i).get("telephone").toString();
                items.push({
                    bindName : {
                        text : name
                    },
                    bindType : {
                        text : "Class : " + type
                    },
                    bindTelephone : {
                        text : "Telp : " + telephone
                    }
                });
                section.setItems(items);
                sections.push(section);
            }
            $.listView.sections = sections;
        }
    }
}

function onClickSendEmail() {
    
    var emailDialog = Ti.UI.createEmailDialog();
    emailDialog.subject = "Hello from Titanium";
    emailDialog.toRecipients = ['foo@yahoo.com'];
    emailDialog.messageBody = '<b>Appcelerator Titanium Rocks!</b>';
    var f = Ti.Filesystem.getFile('cricket.wav');
    emailDialog.addAttachment(f);
    emailDialog.open();
}

load();
