var args = $.args;
var moment = require('alloy/moment');
var toast = Ti.UI.createNotification({
    message : "Visit Plan berhasil ..",
    duration : Ti.UI.NOTIFICATION_DURATION_LONG
});
var collection_customer = Alloy.createCollection('customer');
var collection_callplan_header = Alloy.createCollection('callplan_header');
var collection_callplan_item = Alloy.createCollection('callplan_item');
var collection_callplan_cust = Alloy.createCollection('callplan_cust');
var win = $.win;
var arrLocation = [];
var arrId = [];
var arrPlace = [];
var arrName = [];
var arrIdCust = [];

var tempIdSelected;
var nameSelected = [];
var idSelected = [];

var arrDay = ["All Day", "Morning", "Afternoon", "Custom"];
var arrAgenda = ["Call", "Seminar", "Personal Arrangement", "Other Event"];
var arrFile = ["nodejs.pdf", "tutoApp.pdf", "artikel.pdf", "report.pdf"];
var idCustomer,
    place;
var call_plan_date;
var call_plan_time;
var date_selected;
var from_time = "",
    to_time = "";
function onClickFromDate() {
    var picker = Ti.UI.createPicker({
        type : Ti.UI.PICKER_TYPE_DATE
    });
    picker.showDatePickerDialog({
        minDate : new Date(),
        // maxDate : new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), 22, 0, 0),
        value : new Date(),
        callback : function(e) {
            if (e.cancel) {
                Ti.API.info('user canceled dialog');
            } else {

                var day = moment(e.value).format('ddd');
                var date = moment(e.value).format('LL');
                $.lblFromDate.text = day + ", " + date;

                call_plan_date = date;
                date_selected = e.value;
            }
        }
    });
}

function onClickVisitTime() {
    var option = {
        options : arrDay,
        cancel : 5,
        // selectedIndex: 0,
    };
    var dialog = Ti.UI.createOptionDialog(option);
    dialog.show();
    dialog.addEventListener('click', function(e) {

        if (e.index === e.source.cancel || e.index === e.cancel) {
            Ti.API.info('The cancel button was clicked');
        } else {
            $.lblVisitTime.text = arrDay[e.index];
        }

        if (e.index == 3) {
            $.viewCustomTime.visible = true;
        } else {

            $.viewCustomTime.visible = false;
        }
        if (e.index == 0) {
            //allday
            var date = date_selected;
            var d1 = moment(date.setHours(8, 10)).format('HH:mm');
            var d2 = moment(date.setHours(17, 10)).format('HH:mm');

            var allday = d1 + " - " + d2;

            call_plan_time = allday;
            // console.log("callfromtime m " + call_plan_time);
        }
        if (e.index == 1) {
            //morning
            var date = date_selected;
            var d1 = moment(date.setHours(8, 10)).format('HH:mm');
            var d3 = moment(date.setHours(13, 10)).format('HH:mm');
            var morning = d1 + " - " + d3;
            call_plan_time = morning;
            // console.log("callfromtime m " + call_plan_time);

        }
        if (e.index == 2) {
            //afternoon
            var date = date_selected;
            var d2 = moment(date.setHours(17, 10)).format('HH:mm');
            var d4 = moment(date.setHours(13, 30)).format('HH:mm');

            var afternoon = d4 + " - " + d2;
            call_plan_time = afternoon;
            //console.log("callfromtime m " + call_plan_time);
        }
    });
}

function onClickFromTime() {

    var picker = Ti.UI.createPicker({
        type : Ti.UI.PICKER_TYPE_TIME
    });
    picker.showTimePickerDialog({
        format24 : true,
        // minDate : new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), 9, 0, 0),
        // maxDate : new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), 22, 0, 0),
        value : new Date(), //new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), now.getHours(), now.getMinutes()+30, now.getSeconds()),
        callback : function(e) {
            if (e.cancel) {
                Ti.API.info('user canceled dialog');
            } else {
                var time = moment(e.value).format('HH:mm');
                // timeSelected = time;
                $.lblFromTime.text = time;
                from_time = time;
            }
        }
    });
}

function onClickToDate() {
    var picker = Ti.UI.createPicker({
        type : Ti.UI.PICKER_TYPE_DATE
    });
    picker.showDatePickerDialog({
        minDate : new Date(),
        // maxDate : new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), 22, 0, 0),
        value : new Date(),
        callback : function(e) {
            if (e.cancel) {
                Ti.API.info('user canceled dialog');
            } else {
                var day = moment(e.value).format('ddd');
                var date = moment(e.value).format('LL');
                $.lblToDate.text = day + ", " + date;
            }
        }
    });
}

function onClickToTime() {

    var picker = Ti.UI.createPicker({
        type : Ti.UI.PICKER_TYPE_TIME
    });
    picker.showTimePickerDialog({
        format24 : true,
        // minDate : new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), 9, 0, 0),
        // maxDate : new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), 22, 0, 0),
        value : new Date(), //new Date(now.getFullYear(), now.getMonth() + 1, now.getDate(), now.getHours(), now.getMinutes()+30, now.getSeconds()),
        callback : function(e) {
            if (e.cancel) {
                Ti.API.info('user canceled dialog');
            } else {
                var time = moment(e.value).format('HH:mm');

                $.lblToTime.text = time;
                to_time = time;
            }
        }
    });

}

function onClickLocation() {
    console.log("addschedule onclick location " + JSON.stringify(arrLocation));
    var option = {
        options : arrLocation
    };

    var dialog = Ti.UI.createOptionDialog(option);
    dialog.show();
    dialog.addEventListener('click', function(e) {
        idCustomer = arrId[e.index];
        place = arrLocation[e.index];
        $.txtEventLocation.value = arrLocation[e.index];

    });
}

function onClickDescription() {
    var option = {
        options : arrAgenda
    };
    var dialog = Ti.UI.createOptionDialog(option);
    dialog.show();
    dialog.addEventListener('click', function(e) {
        $.txtEventDescription.value = arrAgenda[e.index];

    });
}

$.on("setGuest", setGuest);
function setGuest() {
    collection_customer.fetch({
        query : "select id, name from customer where ischecked = 1"
    });
    arrName = [];
    arrIdCust = [];

    for (var i = 0; i < collection_customer.length; i++) {
        arrName.push(collection_customer.at(i).get('name').toString());
        arrIdCust.push(collection_customer.at(i).get('id'));
    }
    $.txtEventGuest.value = arrName.toString();
}

function onClickGuest() {
    var args = {
        place : place,
        parent : $
    };
    Alloy.createController("listCustomer", args).getView().open();

}

function onClickDone() {

    var date = new Date();
    collection_customer.updateAllStatus();
    if (from_time != "") {
        call_plan_time = from_time + " - " + to_time;
    }
    // console.log($.txtEventDescription.value);
    var model_callplan_item = Alloy.createModel('callplan_item', {
        event_name : $.txtEventName.value,
        visit_date : call_plan_date,
        visit_time : call_plan_time,
        agenda : $.txtEventDescription.value,
        notes : '',
        status : 0,
        submited_time : '',
        signature : '',
        submited_latitude : '',
        submited_longitude : '',
        created_date : '',
        is_approved : 0,
        id_user : 'user1',
        approved_by : ''
    });
    model_callplan_item.save();
    collection_callplan_item.fetch({
        silent : true,
        query : "select id from callplan_item where visit_date ='" + call_plan_date + "' and visit_time = '" + call_plan_time + "'"
    });
    var idCal = collection_callplan_item.at(0).get("id");
    var model_callplan_cust;
    for (var i = 0; i < arrIdCust.length; i++) {
        model_callplan_cust = Alloy.createModel('callplan_cust', {
            id_customer : arrIdCust[i],
            id_callplan : idCal,
            signature : ''
        });
        model_callplan_cust.save();
    }

    win.close();
}

function onClickCancel() {
    win.close();
}
var count=10;
function onClickAddFile() {
    var fileName;
    var option = {
        options : arrFile,
        cancel : 5,

    };
    var dialog = Ti.UI.createOptionDialog(option);
    dialog.show();
    dialog.addEventListener('click', function(e) {
        if (e.index === e.source.cancel || e.index === e.cancel) {
            console.log("The cancel button was clicked");
        } else {

        }
        if (e.index == 0) {
            fileName = arrFile[e.index];
            
            var imgFile = Ti.UI.createImageView({
               height :  64,
               width : 64,
               image :  "/docicon.png",
               left : count
            });
            
            var lblFileName = Ti.UI.createLabel({
               text : fileName ,
               left : count,
               bottom : 0
            });
            $.viewIconName.add(imgFile);
            $.viewIconName.add(lblFileName);
            // $.viewIconName.left = count;
            count = count +90;
            
        }
        if (e.index == 1) {
            fileName = arrFile[e.index];
            
            var imgFile = Ti.UI.createImageView({
               height :  64,
               width : 64,
               image :  "/docicon.png",
               left : count
            });
            
            var lblFileName = Ti.UI.createLabel({
               text : fileName ,
               left : count,
               bottom : 0
            });
            $.viewIconName.add(imgFile);
            $.viewIconName.add(lblFileName);
            // $.viewIconName.left = count;
            count = count +90;
        }
        if (e.index == 2) {
            fileName = arrFile[e.index];
            
            var imgFile = Ti.UI.createImageView({
               height :  64,
               width : 64,
               image :  "/docicon.png",
               left : count
            });
            
            var lblFileName = Ti.UI.createLabel({
               text : fileName ,
               left : count,
               bottom : 0,
            });
            $.viewIconName.add(imgFile);
            $.viewIconName.add(lblFileName);
            // $.viewIconName.left = count;
            count = count +90;
        }
        if (e.index == 3) {
            fileName = arrFile[e.index];
            
            var imgFile = Ti.UI.createImageView({
               height :  64,
               width : 64,
               image :  "/docicon.png",
               left : count
            });
            
            var lblFileName = Ti.UI.createLabel({
               text : fileName ,
               left : count,
               bottom : 0
            });
            $.viewIconName.add(imgFile);
            $.viewIconName.add(lblFileName);
            // $.viewIconName.left = count;
            count = count +90;
        }
    });
}

function init() {
    var now = new Date();
    var date = moment(now).format('ddd') + ", " + moment(now).format('LL');
    date_selected = now;
    var time = moment(now).format('HH:mm');
    var d1 = moment(now.setHours(8, 10)).format('HH:mm');
    var d2 = moment(now.setHours(17, 10)).format('HH:mm');
    var d3 = moment(now.setHours(13, 10)).format('HH:mm');
    var d4 = moment(now.setHours(13, 30)).format('HH:mm');
    var allday = d1 + " - " + d2;
    var morning = d1 + " - " + d3;
    var afternoon = d4 + " - " + d2;
    call_plan_time = allday;
    $.lblFromDate.text = date;
    $.lblFromTime.text = time;
    $.lblToTime.text = time;
    collection_customer.fetch({
        query : "select id, place from customer group by place"
    });
    for (var i = 0; i < collection_customer.length; i++) {
        arrLocation.push(collection_customer.at(i).get('place').toString());
        arrId.push(collection_customer.at(i).get('id').toString());

    }
}

init();
function clear() {
    $.txtEventName.value = "";
    $.txtEventLocation.value = "";
    $.txtEventGuest.value = "";
    $.txtEventDescription.value = "";
}

$.on("clear", clear);
$.on("done", function(e) {
    var date = new Date();
    if ($.txtEventName.value != "") {
        try {

            collection_customer.updateAllStatus();
            if (from_time != "") {
                call_plan_time = from_time + " - " + to_time;
            }
            var model_callplan_item = Alloy.createModel('callplan_item', {
                event_name : $.txtEventName.value,
                visit_date : call_plan_date,
                visit_time : call_plan_time,
                agenda : $.txtEventDescription.value,
                notes : '',
                status : 0,
                submited_time : '',
                signature : '',
                submited_latitude : '',
                submited_longitude : '',
                created_date : '',
                is_approved : 0,
                id_user : 'user1',
                approved_by : ''
            });
            model_callplan_item.save();
            collection_callplan_item.fetch({
                silent : true,
                query : "select id from callplan_item where visit_date ='" + call_plan_date + "' and visit_time = '" + call_plan_time + "'"
            });
            var idCal = collection_callplan_item.at(0).get("id");
            var model_callplan_cust;
            for (var i = 0; i < arrIdCust.length; i++) {
                model_callplan_cust = Alloy.createModel('callplan_cust', {
                    id_customer : arrIdCust[i],
                    id_callplan : idCal,
                    signature : ''
                });
                model_callplan_cust.save();
            }
            console.log("Berhasil");
            clear();

            toast.show();
        } catch(err) {
            console.log(err);
        }

    } else {
        alert("Mohon isi field yang disediakan");
    }

});
